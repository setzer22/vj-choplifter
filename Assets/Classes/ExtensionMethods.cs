﻿using System.Collections;
using UnityEngine;

public static class ExtensionMethods {

    public static bool near (this float f, float target, float epsilon) {
        return target + epsilon > f && target - epsilon < f; 
    }

    public static float sqr (this float x) {
        return x*x;
    }
}
