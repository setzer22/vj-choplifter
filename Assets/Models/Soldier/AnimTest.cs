﻿using UnityEngine;
using System.Collections;

public class AnimTest : MonoBehaviour {

	public Animator anim;
	public Renderer gun;
	public BulletAnimator bulletAnimator;

	// Use this for initialization
	void Start () {
		anim.GetBehaviour<ShootAnim>().gun = gun;
		anim.GetBehaviour<ShootAnim>().bulletAnimator = bulletAnimator;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.X)) anim.SetBool("shooting", true);
		else anim.SetBool("shooting", false);

		if(Input.GetKey(KeyCode.LeftArrow)) {
			transform.Translate(new Vector3(0, 0, 5*Time.deltaTime));
			transform.LookAt (transform.position + new Vector3(0,0,1));
			anim.SetBool("moving", true);
		}
		else if(Input.GetKey(KeyCode.RightArrow)) {
			transform.Translate(new Vector3(0, 0, 5*Time.deltaTime));
			transform.LookAt (transform.position + new Vector3(0,0,-1));
			anim.SetBool("moving", true);
		}
		else anim.SetBool("moving", false);
	}
}
