﻿using UnityEngine;
using System.Collections;

public class BaseController : MonoBehaviour {

	public int freeSoldiers; //Numeber of soldiers on freedom. "The puntuation in this game".

	public int maxSoldiers;

	public RescuedSoldiers RS;


	public Transform soldierPrefab;
	public RescuedSoldiers bController;
	public Helicopter_Controller hController;
	public Winner wi;

    bool won = false;

	public void liberate() {
		++freeSoldiers;
	}

	void Update() {
		if (!won && freeSoldiers == maxSoldiers) {
            won = true;
			wi.passLevel();
		}

		else if (hController.colision == Helicopter_Controller.Colision.Base && 
			bController.soldiers > 0 &&
			!hController.poolSoldiers.activeSelf) {
			//position
			hController.poolSoldiers.transform.position = new Vector3(hController.gameObject.transform.position.x+4.2f, transform.position.y+0.5f, hController.gameObject.transform.position.z-0.5f);
			hController.poolSoldiers.GetComponent<RescuedSoldiers_Controller>().recalculateVec();

			//active
			hController.poolSoldiers.SetActive(true);
		}
	}
	

	void OnGUI() {
		GUI.backgroundColor = Color.blue;
		GUI.Button(new Rect(Screen.width-200, 10, 150, 25), "FreeSoldiers x " + freeSoldiers);
	}
}
