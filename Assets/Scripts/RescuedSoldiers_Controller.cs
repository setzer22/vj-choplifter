using UnityEngine;
using System.Collections;

public class RescuedSoldiers_Controller : MonoBehaviour
{
	//The main target for the AI.
	public Transform target;
	public Transform base_;

	public Helicopter_Controller hController;
	public RescueZone RZ;
	
	public Animator anim;

	public CharacterController controller;

	private float direction;

	public bool isFree = true;

	public float speed = 0.5f;
	private bool moving = false;
	private Vector3 vec;

	void Start() {
		direction = (target.position.x - transform.position.x) > 0? -1.0f: 1.0f;
		vec = transform.position - new Vector3(base_.position.x,base_.position.y,base_.position.z-3.0f);
	}

	void Update () {
		anim.SetBool("moving", moving);

		if (transform.position.x - 3.5f <= target.position.x) {
			hController.gameObject.GetComponent<RescuedSoldiers> ().rescueSoldier ();
			RZ.liberate ();
			beSave ();
		}
	}

	public void goHome() {
		isFree = true;
		walk(-direction);
	}

	public void goToChop() {
		isFree = false;
		vec = target.position - transform.position;
		walk(direction);
	}

	private void walk (float to_direction) {
		if (isFree) {
			transform.LookAt (transform.position - vec);
		} else {
			transform.LookAt (transform.position + vec);
		}
		moving = true;
		controller.Move(new Vector3(vec.x*to_direction,0.0f,vec.z*to_direction)*speed*Time.deltaTime);
	}

	public void beSave() {
		gameObject.SetActive(false); //desactive.
		gameObject.transform.position = new Vector3 (0.0f,0.0f,0.0f); //hide on the position(0,0,0).
	}

	public void recalculateVec() {
		vec = transform.position - new Vector3(base_.position.x,base_.position.y,base_.position.z-3.0f);
	}
}

