﻿using UnityEngine;
using System.Collections;

public class HelicopterDeathMotion : MonoBehaviour {

    public Rigidbody body;
    public Transform force_point;
    public float delayDeath = 1.0f;

    public GameObject explosion;

    public IEnumerator deferDeath(GameObject go) {
        yield return new WaitForSeconds(delayDeath); 
        GameObject.Destroy(go);
        
    }

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody>();
        GameObject e = (GameObject)Instantiate(explosion, force_point.transform.position, force_point.transform.rotation);
        e.transform.localScale = 0.5f * e.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 force = 5 * force_point.TransformDirection(0,0,1);
        body.AddForceAtPosition( force, force_point.position);
        body.AddForce( new Vector3(1.0f, -1.0f, 1.0f));
	}

    void OnCollisionEnter(Collision c) {
        Instantiate(explosion, transform.position, transform.rotation);
        StartCoroutine(deferDeath(gameObject));
    }




}
