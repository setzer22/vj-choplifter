using UnityEngine;
using System.Collections;

public class TankDeath : MonoBehaviour
{

    public GameObject tank_dead_prefab;
    bool dead = false;

	void die() {
        if (dead) return;
        dead = true;

        Instantiate(tank_dead_prefab, transform.position, transform.rotation);
		GameObject.Destroy (gameObject);
	}
}

