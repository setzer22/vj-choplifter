using UnityEngine;
using System.Collections;

public class AI_Tank : MonoBehaviour
{

	//Tank Controller:
	public AA_Tank_Controller controller;

	//The distance from target that active the moving mode.
	public float dist_move;

	//The distance from target that active the shooting mode
	public float dist_shoot;

	private enum State  {
		IDLE, MOVE, SHOOT
	}

	private State state;
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 me = transform.position;
		Vector3 him = controller.target.position; //The chopter position.

		me.y = him.y = 0.0f; //Only calculate the distance in Z axis.

		float relativeDist = (him - me).sqrMagnitude;

		if (relativeDist <= dist_move*dist_move && relativeDist >= dist_shoot*dist_shoot) {
			state = State.MOVE;
		} else if(relativeDist <= dist_shoot*dist_shoot) {
			state = State.SHOOT;
		} else {
			state = State.IDLE;
		}

		switch (state) {
		case State.MOVE:
			if(me.z > him.z) controller.moveRight();
			else controller.moveLeft();
			break;
		case State.SHOOT:
			controller.shoot();
			break;
		//default: State.IDLE
		}

	}
}

