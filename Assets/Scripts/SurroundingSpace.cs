﻿using UnityEngine;
using System.Collections;

public class SurroundingSpace : MonoBehaviour {

    AI_Soldier AI;

    void Start() {
        AI = transform.parent.gameObject.GetComponent<AI_Soldier>();    
    }

    void OnTriggerEnter(Collider other) {
        if(other.tag == "Enemy" && 
           other.GetComponent<AI_Soldier>() != null) {
            AI.add_nearby(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other) {
        if(other.tag == "Enemy" && 
           other.GetComponent<AI_Soldier>() != null) {
            AI.remove_nearby(other.gameObject); 
        }
        
    }

}
