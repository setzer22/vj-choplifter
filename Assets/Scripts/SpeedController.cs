using UnityEngine;
using System.Collections;

public class SpeedController : MonoBehaviour
{
	public float powerVelocity;

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			other.gameObject.SendMessage("powerSpeed",powerVelocity);
			GameObject.Destroy(gameObject);
		}
	}
}

