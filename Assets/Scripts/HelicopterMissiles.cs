﻿using UnityEngine;
using System.Collections;

public class HelicopterMissiles : MonoBehaviour {

    public float missile_damage = 5.0f;

    public GameObject missile_prefab;

    public Transform launch_position_left;
    public Transform launch_position_right;

    public bool alternate = true;
    public bool left = false;

	void Start () {
	
	}

    void shoot_missile() {
        Vector3 launch_dir = new Vector3(-transform.right.x, 0, -transform.right.z);
        GameObject missile = (GameObject) Instantiate( 
            missile_prefab, 
            (left ? launch_position_left : 
                    launch_position_right).position,  
            Quaternion.LookRotation(launch_dir)
        );

        if (alternate) left = !left;
        missile.GetComponent<MissileTest>().damage = missile_damage;


    }

    int min (int a, int b) {return a < b ? a : b;}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetMouseButtonDown(1)) {
            shoot_missile();    
        }
	}
}
