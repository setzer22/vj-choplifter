﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

	public float max_speed;
	public float acceleration;
	public Vector3 axis;
	public bool right;
    public bool reduce = false; 

	private float speed;

	void Start () {
		speed = 0;
	}
	
	void Update () {
        if(!reduce) {
            speed += acceleration*Time.deltaTime;
            if (speed > max_speed) speed = max_speed;
        }
        else {
            speed -= acceleration*Time.deltaTime;
            if(speed < 0) speed = 0;
        }
        transform.Rotate((right?1.0f:-1.0f)*speed*Time.deltaTime*(axis.normalized));
	}

	public float getSpeed() {return speed;}
}
