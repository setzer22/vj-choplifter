﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

    public float delay = 10.0f;

    public IEnumerator game_over() {
        SendMessage("gameOverMessage");
        yield return new WaitForSeconds(delay);
        Application.LoadLevel("GameOver");
    }

    public void gameOver() {
        StartCoroutine(game_over());
    }
}
