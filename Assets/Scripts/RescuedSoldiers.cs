﻿using UnityEngine;
using System.Collections;

public class RescuedSoldiers : MonoBehaviour {

    public int soldiers = 0;
    

    public void rescueSoldier() {
        ++soldiers;
    }

	public void freeSoldier() {
		--soldiers;
		if (soldiers < 0) soldiers = 0;
	}

    void OnGUI() {
        GUI.backgroundColor = Color.red;
        GUI.Button(new Rect(10, 50, 150, 25), "Soldiers x " + soldiers);
    }
}
