﻿using UnityEngine;
using System.Collections;

public class AA_Tank_Controller : MonoBehaviour {

	public Transform body;
	public Transform turret;
	public Transform cannon;
	public Transform target;
	public BulletAnimator bulletAnimator1;
	public BulletAnimator bulletAnimator2;

	public float speed;

    //Unity is inconsistent with this. rigidbody is still a member
    //variable for GameObject, so this declarationthrows warnings 
    //but its use is deprecated and trying to use rigidbody is a 
    //compiler error, hence the "new"
	private new Rigidbody rigidbody;

	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	void Update () {
		Vector3 targetY0 = target.position;
		Vector3 turretY0 = turret.position;
		targetY0.y = turretY0.y = 0;
		Quaternion targetRotation = Quaternion.LookRotation(turretY0 - targetY0, Vector3.up);
		Quaternion rot = Quaternion.Slerp(turret.rotation, targetRotation, Time.deltaTime*speed);
		turret.eulerAngles = new Vector3(turret.eulerAngles.x, rot.eulerAngles.y, turret.eulerAngles.z);
		
		Vector3 targetX0 = target.position;
		Vector3 cannonX0 = cannon.position;
		cannonX0.x = targetX0.x = 0;
		Quaternion targetRotation_cannon = Quaternion.LookRotation(targetX0 - cannonX0, Vector3.up);
		Quaternion rot_c = Quaternion.Slerp(cannon.rotation, targetRotation_cannon, Time.deltaTime*speed);
		cannon.eulerAngles = new Vector3(rot_c.eulerAngles.x, cannon.eulerAngles.y, cannon.eulerAngles.z);
	}

	public void moveRight() {
		move (new Vector3(0.0f,0.0f,-1.0f));
	}

	public void moveLeft() {
		move (new Vector3 (0.0f, 0.0f, 1.0f));
	}

	private void move(Vector3 dir) {
		bulletAnimator1.hidden = bulletAnimator2.hidden = true;
		rigidbody.AddForce (dir*speed*Time.deltaTime);
	}

	public void shoot() {
		if(bulletAnimator1.hidden == true) bulletAnimator1.hidden = false;
		if(bulletAnimator2.hidden == true) bulletAnimator2.hidden = false;
	}
}
