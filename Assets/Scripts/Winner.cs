using UnityEngine;
using System.Collections;

public class Winner : MonoBehaviour
{
    public GameObject messages;

	public float delay = 5.0f;

	public IEnumerator win() {
        messages.SendMessage("winMessage");
		yield return new WaitForSeconds (delay);
		if (Application.loadedLevelName == "jungle") {
			Application.LoadLevel ("desert");
		} else {
			Application.LoadLevel("MainMenu");
		}
	}
	
	public void passLevel() {
		StartCoroutine (win ());
	}
}

