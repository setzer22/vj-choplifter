using UnityEngine;
using System.Collections;

public class GasController : MonoBehaviour
{
	public float maxGas;
	public float GasPerFrame;

	private float currentGas;

	void Start() {
		currentGas = maxGas;
	}

	void rechargeGas() {
		currentGas += GasPerFrame;
		if(currentGas > maxGas) currentGas = maxGas;
	}

	public void useGas() {
		currentGas -= 0.015f;
		if (currentGas < 0) {
            currentGas = 0.0f;
            gameObject.SendMessage("outOfFuel");
        }
	}

	void OnGUI() {
		GUI.backgroundColor = Color.yellow;
		int length = (int) (200 * currentGas / maxGas);
		GUI.Button(new Rect(10, 80, length, 25), "Gasoline");
	}
}

