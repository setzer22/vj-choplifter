﻿using UnityEngine;
using System.Collections;

public class BulletAnimator : MonoBehaviour {

	public new Renderer renderer;
	public Transform mesh;
	public ParticleSystem hitparticles;
	public float speed = 1.0f;
    public float parentScale = 1.0f;
    public bool is_enemy = true;

    float max_volume;

	Material material;

    public float damagePerSecond = 1.0f;

	float max_length = 200.0f;
	float offset = 0.0f;

	public bool hidden {
		get {return _hidden;} 
		set  {
			_hidden = value;
			if(value) {
				renderer.enabled = false;
				hitparticles.enableEmission = false;
			}
			else {
				renderer.enabled = true;
				offset = 0.0f;
                if(GetComponent<AudioSource>() != null) {
                    GetComponent<AudioSource>().volume = max_volume;
                    GetComponent<AudioSource>().Play();
                }
			}
		}
	}
	bool _hidden = false;

	void Start () {
		hidden = true;
        material = renderer.material;
        if(GetComponent<AudioSource>() != null) {
            max_volume = GetComponent<AudioSource>().volume;
            GetComponent<AudioSource>().volume = 0;
        }

	}
	
	void Update () {
		if(_hidden) {
            if(GetComponent<AudioSource>() != null) {
                AudioSource src = GetComponent<AudioSource>();
                float volume = src.volume - Time.deltaTime*3;
                src.volume = volume;
            }    
            return;
        }
		material.SetTextureScale("_MainTex", new Vector2(1, 0.1f*mesh.transform.localScale.x));

		offset += Time.deltaTime * speed;
		material.SetTextureOffset("_MainTex", new Vector2(0, offset));

        if(!is_enemy) return; //Only the enemies handle its raycast logic here. 
                              //For the helicopter, this is handled in the laser code

		RaycastHit hitInfo;
		//Debug.DrawRay (mesh.transform.position, (mesh.transform.position - 50*mesh.transform.right) - mesh.transform.position, Color.blue);
		Physics.Raycast(mesh.transform.position, (mesh.transform.position - mesh.transform.right) - mesh.transform.position, out hitInfo, 400); 
		if(hitInfo.collider != null && hitInfo.collider.tag == "Player") {
			float len = (hitInfo.point - mesh.transform.position).magnitude / (2*parentScale);
			mesh.transform.localScale = new Vector3(len, mesh.transform.localScale.y, mesh.transform.localScale.z);
			hitparticles.transform.position = hitInfo.point;
			hitparticles.transform.rotation = Quaternion.LookRotation(mesh.transform.position - hitInfo.point);
			hitparticles.enableEmission = true;

            hitInfo.collider.gameObject.SendMessage("damage", damagePerSecond * Time.deltaTime); 
		}
		else {
			hitparticles.enableEmission = false;
			mesh.transform.localScale = new Vector3(max_length, mesh.transform.localScale.y, mesh.transform.localScale.z);
		}
	}

}
