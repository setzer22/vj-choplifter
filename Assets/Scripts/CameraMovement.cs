﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public GameObject follow;
	public Vector3 relativePos;

	// Use this for initialization
	void Start () {
		transform.position = follow.transform.position + relativePos;	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = follow.transform.position + relativePos;	
		transform.LookAt(follow.transform.position);
	}
}
