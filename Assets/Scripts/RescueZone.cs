﻿using UnityEngine;
using System.Collections;

public class RescueZone : MonoBehaviour {

    public int current_soldiers;

	public void liberate() {
		--current_soldiers;
		if (current_soldiers < 0) current_soldiers = 0;
	}
}
