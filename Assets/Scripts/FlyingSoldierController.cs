﻿using UnityEngine;
using System.Collections;

public class FlyingSoldierController : MonoBehaviour {

    public float current_height;
    public float oscillation_amplitude;
    public float oscillation_speed;
    public float cadence;
    public float fire_treshold;
    public float fire_dist;

    public GameObject missile_prefab;

    public Transform target;
    public Transform bazooka;

    float time = 0;
    public float shoot_timer = 0; 
    float unique_offset;

	void Start () {
        current_height = transform.position.y;
        //We introduce a random starting angle value for the sin function so 
        //soldiers don't oscillate perfectly synchronised, which looks bad
        unique_offset = Random.Range(0.0f, oscillation_speed);
	}
	
	void Update () {
        time += Time.deltaTime;
        shoot_timer -= Time.deltaTime; if(shoot_timer < 0) shoot_timer = 0;

        Vector3 pos = transform.position;
        pos.y = current_height + (oscillation_amplitude * Mathf.Sin(unique_offset + time*oscillation_speed));

        Vector3 look_dir = new Vector3(pos.x, pos.y, target.position.z - pos.z);
        transform.LookAt(look_dir);

        float dist = (transform.position - target.position).sqrMagnitude;
        if(fire_dist.sqr() > dist && pos.y.near(target.position.y, fire_treshold) && shoot_timer <= 0) {
            shoot();   
        } 

        transform.position = pos;
	
	}

    void shoot() {
        shoot_timer = cadence;
        Instantiate(missile_prefab, 
                    bazooka.transform.position, 
                    bazooka.transform.rotation);
    }
}
