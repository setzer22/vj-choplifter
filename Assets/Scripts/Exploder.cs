﻿using UnityEngine;
using System.Collections;

public class Exploder : MonoBehaviour {

    public Rigidbody body;
    public Rigidbody turret;
    public GameObject explosion;

	// Use this for initialization
	void Start () {
        Vector3 offset = Random.insideUnitCircle;
        body.AddExplosionForce(500, transform.position + offset, 10f); 
        turret.AddExplosionForce(500, transform.position + offset, 10f); 
        if(explosion != null) {
            Instantiate(explosion, transform.position, transform.rotation);
        }
	}
	
}
