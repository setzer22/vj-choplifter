﻿using UnityEngine;
using System.Collections;

public class HitPoints : MonoBehaviour {

    public float max_hit_points;
    public float current_hit_points;

    public bool show_bar = false;

	void Start () {
	
	}
	
	void Update () {
	
	}

    void damage(float hit_damage) {
        current_hit_points -= hit_damage;
        if(current_hit_points <= 0) gameObject.SendMessage("die");
    }

    float min(float x, float y) {return x < y ? x : y;}

    void heal(float heal_damage) {
        current_hit_points = min(max_hit_points, current_hit_points + heal_damage);
    }

    void OnGUI() {
        if(!show_bar) return;
        GUI.backgroundColor = Color.green;
        int length = (int) (200 * current_hit_points / max_hit_points);
        GUI.Button(new Rect(10,10, length, 25), "HP");
    }
}
