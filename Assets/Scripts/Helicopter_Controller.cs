using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Helicopter_Controller : MonoBehaviour
{
	public Transform soldierPrefab;

	public BaseController b;

	public enum Colision {
		Base, Prision, Nothing
	};
	
	public Colision colision;

	public GameObject poolSoldiers;

	public GameObject thisPrision;
	
	void Start ()
	{
		thisPrision = null;
		colision = Colision.Nothing;
		Transform t = Instantiate(soldierPrefab, new Vector3(0.0f, 0.0f, 0.0f), transform.rotation) as Transform;
		poolSoldiers = t.gameObject;
		poolSoldiers.SetActive(false);
		
		poolSoldiers.GetComponent<RescuedSoldiers_Controller>().target = gameObject.transform;
		poolSoldiers.GetComponent<RescuedSoldiers_Controller>().hController = gameObject.GetComponent<Helicopter_Controller>();
		poolSoldiers.GetComponent<AI_RescuedSoldiers>().hController = gameObject.GetComponent<Helicopter_Controller>();
		poolSoldiers.GetComponent<RescuedSoldiers_Controller>().isFree = true;
		poolSoldiers.GetComponent<RescuedSoldiers_Controller>().speed = 0.5f;
		poolSoldiers.GetComponent<collider_Rescued> ().b = b;
		poolSoldiers.GetComponent<collider_Rescued> ().RS = gameObject.GetComponent<RescuedSoldiers> ();
		poolSoldiers.GetComponent<RescuedSoldiers_Controller> ().base_ = GameObject.FindGameObjectWithTag("house").transform;
	}

	//Detect the colision thing.
	void OnTriggerEnter(Collider other) {
		if (other.tag == "base") {
			colision = Colision.Base;
		}
		else if (other.tag == "prision") { 
			colision = Colision.Prision;
			thisPrision = other.gameObject;
			poolSoldiers.GetComponent<collider_Rescued>().RZ = other.gameObject.GetComponent<RescueZone>();
			poolSoldiers.GetComponent<RescuedSoldiers_Controller>().RZ = other.gameObject.GetComponent<RescueZone>();
		}
	}

	void OnTriggerExit(Collider other) {
		colision = Colision.Nothing;
		thisPrision = null;
	}
}

