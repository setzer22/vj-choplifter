﻿using UnityEngine;
using System.Collections;

public class HelicopterDeath : MonoBehaviour {

    public GameObject deadPrefab;
    public GameOver gameOverComponent;

    bool dead = false;

    public void die() {
        if(dead) return;
        dead = true;

        Instantiate(deadPrefab, transform.position, transform.rotation);
        gameObject.SetActive(false);
        gameOverComponent.gameOver();
    }

    public void outOfFuel() {
        GetComponent<Movement>().enabled = false;
        Rotator[] rotators = GetComponentsInChildren<Rotator>();
        foreach (Rotator c in rotators) c.reduce = true;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().AddForce(-5*Vector3.up, ForceMode.Impulse);
        GetComponent<Rigidbody>().drag = 0;

        gameOverComponent.gameOver();
    }
}
