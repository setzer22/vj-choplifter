﻿using UnityEngine;
using System.Collections;

public class SoldierController : MonoBehaviour {

    //The movement speed
    public float speed = 5.0f;

    //Reaction time from move to shoot
    public float move_to_shoot_reaction_time = 0.5f;

    //Reaction time from shoot to move
    public float shoot_to_move_reaction_time = 0.5f;

    //This character's animator
	public Animator anim;

    //A reference to the gun renderer just so we can forward it to the animator
	public Renderer gun;

    //The bullet animator for this soldier
	public BulletAnimator bulletAnimator;

    public CharacterController controller;

    private bool moving, shooting;

    private Vector3 direction;

    private bool blocked;

    // ===========================
    // Commands for the controller
    // ===========================

    delegate void DelayDelegate();
    IEnumerator delay(float seconds, DelayDelegate func) {
        yield return new WaitForSeconds(seconds);
        func(); 
        blocked = false;
    }

    public void walkLeft() {
        if(blocked) return;
        
        walk(-Vector3.forward);
    }

    public void walkRight() {
        if(blocked) return;

        walk(Vector3.forward);
    }

    private void walk (Vector3 to_direction) {
        DelayDelegate actions = () => {
            transform.LookAt(transform.position + to_direction);
            moving = true;
            direction = to_direction;
        };
        
        if(shooting) {
            blocked = true;
            StartCoroutine(delay(shoot_to_move_reaction_time, actions));
        }
        else {
            actions();
        }

        shooting = false;
    }

    public void lookLeft() {
        if(blocked) return;

        transform.LookAt(transform.position - Vector3.forward);
        moving = false;
    }

    public void lookRight() {
        if(blocked) return;

        transform.LookAt(transform.position + Vector3.forward);
        moving = false;
    }

    public void lookAt(Vector3 position_) {
        if(blocked) return;
        Vector3 position = position_; 
        position.y = transform.position.y;

        transform.LookAt(position);
        moving = false;
    }


    public void stop() {
        if(blocked) return;

        moving = false;
        shooting = false;
        direction = Vector3.zero;
    }

    public void shoot() {
        if(blocked) return;

        DelayDelegate actions = () => {
            shooting = true;
            direction = Vector3.zero;
        };

        if (!shooting) {
            blocked = true;
            StartCoroutine(delay(move_to_shoot_reaction_time, actions));
        }
        else {
            actions();
        }
        moving = false;
    }

    // =================
    //  Component logic
    // =================

	void Start () {
		anim.GetBehaviour<ShootAnim>().gun = gun;
		anim.GetBehaviour<ShootAnim>().bulletAnimator = bulletAnimator;
	}
	
	void Update () {
		anim.SetBool("shooting", shooting);
		anim.SetBool("moving", moving);

        if (moving) {
            controller.Move(Vector3.up * -10 * Time.deltaTime + direction*speed*Time.deltaTime);
        }

	}

}
