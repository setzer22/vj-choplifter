﻿using UnityEngine;
using System.Collections;

public class GasZone : MonoBehaviour {

	void OnTriggerStay(Collider other) {
		if (other.tag == "Player") {
			other.gameObject.SendMessage("rechargeGas");
		}
	}
}
