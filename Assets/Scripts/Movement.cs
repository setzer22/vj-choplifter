﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    public float speed;
    public float backAngle = 330;
    public float frontAngle = 30;
    public float sostre = 20;

	public GasController gcontroller;
	public Rotator rotator;

    new Rigidbody rigidbody;
    bool rest;
    float restTime;
	private float normalSpeed;
	private float elapsedTime;
    
    enum Stat {
		moveLeft, moveRight, resetPos, Idle, rotLefttoRight, rotRighttoLeft, 
    };
    enum Direction {
        Left, Right
    };
    Direction look_dir;
    Stat state;
    float factor;

    void Start () {
        rigidbody = GetComponent<Rigidbody> ();
        look_dir = Direction.Left;
        state = Stat.Idle;
		normalSpeed = speed;
    }

    bool between(float bot, float top, float val) {
        return bot <= val && val <= top;
    }

    bool isRotationState () {
		return state == Stat.rotLefttoRight || state == Stat.rotRighttoLeft;
    }

    float dir (bool l, bool r) { return l ? 1.0f : (r ? -1.0f : 0.0f); }
    
    void FixedUpdate () {
		if (elapsedTime > 0.0f) {
			--elapsedTime;
		} else {
			speed = normalSpeed; //Restore the normal Speed.
		}

		if (rotator.getSpeed() >= rotator.max_speed) {

            float z = transform.localEulerAngles.z;
            float y = transform.localEulerAngles.y;

            //First, we handle movement sepparately, states are only for rotation.
			Vector3 direction;
			if(transform.position.y < sostre) { //Restrict the max Y.
            	direction = new Vector3(0.0f, dir(Input.GetKey(KeyCode.W), Input.GetKey(KeyCode.S)), 
                                            dir(Input.GetKey(KeyCode.A), Input.GetKey(KeyCode.D)) );
			}
			else {
				direction = new Vector3(0.0f, dir(false, Input.GetKey(KeyCode.S)), 
				                                dir(Input.GetKey(KeyCode.A), Input.GetKey(KeyCode.D)) );
			}
            rigidbody.AddForce(direction*Time.deltaTime*speed);
            
            //Input():
            if(!isRotationState()) {
                if(Input.GetKey(KeyCode.D)) state = Stat.moveRight;
                else if(Input.GetKey(KeyCode.A)) state = Stat.moveLeft;

                else if(Input.GetKey(KeyCode.Q) && look_dir == Direction.Right /*!front*/) 
                    state = Stat.rotRighttoLeft;
                else if(Input.GetKey(KeyCode.E) && look_dir == Direction.Left /*!front*/) 
                    state = Stat.rotLefttoRight;
                else state = Stat.resetPos;
            }

			gcontroller.useGas();


            //Update():

            switch(state) {
            case Stat.moveLeft:
				rigidbody.angularDrag = 5.0f;//The solution angles inclination.
				if(between(backAngle,360,z) || between(0, frontAngle, z)) {
                    rigidbody.AddTorque(new Vector3(800.0f* Time.deltaTime,0.0f,0.0f));
                }
                //TODO: For now we're ignoring the factor. once it works we'll make it work again
                //factor = (z < 180)? 1 - (frontAngle - z)/(frontAngle) : 0.0f;
                break;
			case Stat.moveRight:
				rigidbody.angularDrag = 5.0f;
				if(between(backAngle,360,z) || between(0, frontAngle, z)) {
                    rigidbody.AddTorque(new Vector3(-800.0f* Time.deltaTime,0.0f,0.0f));
                }
                //TODO: Same as above
                //factor = (z > 180)? 1 - (z - backAngle)/(360 - backAngle) : 0.0f;
                break;
            case Stat.resetPos:
				rigidbody.angularDrag = 2.0f;//The solution angles inclination.

                float sign = look_dir == Direction.Left ? 1.0f : -1.0f;
                if(between(0,85,z))
                    rigidbody.AddTorque(new Vector3(sign*-900.0f*Time.deltaTime,0.0f,0.0f));
				else if(z >= 355.0f || z <= 5.0f){ //Fix angle
					rigidbody.angularVelocity = new Vector3(0.0f,0.0f,0.0f);
					transform.rotation = Quaternion.Euler(0.0f,transform.localEulerAngles.y,transform.localEulerAngles.z);
				}
                else if(between(265,355,z))
                    rigidbody.AddTorque(new Vector3(sign*900.0f*Time.deltaTime,0.0f,0.0f));
				else if(z >= 355.0f || z <= 5.0f) { //Fix angle
					rigidbody.angularVelocity = new Vector3(0.0f,0.0f,0.0f);
					transform.rotation = Quaternion.Euler(0.0f,transform.localEulerAngles.y,transform.localEulerAngles.z);
				}
                break;

			case Stat.rotRighttoLeft:
                rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | 
                                        RigidbodyConstraints.FreezePositionX; //Unlock RotationY.

             	if(between(0,89,y) || between(260,360,y) || y <= 1.0f) {
                    rigidbody.AddTorque(new Vector3(0.0f,1200.0f*Time.deltaTime,0.0f));
                }
                else if(between(87,92,y)) {//Chopter is in left direction.
                    look_dir = Direction.Left;
                    rigidbody.angularVelocity = new Vector3(0.0f,0.0f,0.0f);// Stop rotation
                    transform.localEulerAngles = new Vector3(transform.localEulerAngles.x,90.0f,
                                                             transform.localEulerAngles.z); //Fix angle.
                    rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | 
                                            RigidbodyConstraints.FreezeRotationY | 
                                            RigidbodyConstraints.FreezePositionX; //Lock rotation Y.
                    state = Stat.Idle;
                }
                break;

			case Stat.rotLefttoRight:
                rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | 
                                        RigidbodyConstraints.FreezePositionX;

                if(between(271,360,y) || between (1,91,y) || y <= 1.0f) {
                    rigidbody.AddTorque(new Vector3(0.0f,-1200.0f*Time.deltaTime,0.0f));
                }
                else if(between(267,271,y)) {//Chopter is in right direction.
                    look_dir = Direction.Right;
                    rigidbody.angularVelocity = new Vector3(0.0f,0.0f,0.0f);
                    transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 270.0f,
                                                             transform.localEulerAngles.z);
                    rigidbody.constraints = RigidbodyConstraints.FreezeRotationX |
                                            RigidbodyConstraints.FreezeRotationY | 
                                            RigidbodyConstraints.FreezePositionX;
                    state = Stat.Idle;
                }
                break;
            //default: //Stat.Idle

            }
        }
    }

	void powerSpeed(float _speed) {
		elapsedTime = 100.0f;
		speed = _speed;
	}
}
