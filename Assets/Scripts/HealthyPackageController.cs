using UnityEngine;
using System.Collections;

public class HealthyPackageController : MonoBehaviour
{
	
	public float HealthyValue;

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			other.gameObject.SendMessage("heal", HealthyValue);
			GameObject.Destroy(gameObject);
		}
	}
}

