﻿using UnityEngine;
using System.Collections;

public class ShootAnim : StateMachineBehaviour {

	public Renderer gun;
	public BulletAnimator bulletAnimator;
	public float delay = 0.25f;
	float count = 0.0f;
	bool done = false;

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		count += Time.deltaTime;
		if(count > delay && !done) {
			gun.enabled = true;
			bulletAnimator.hidden = false;
			done = true;
		}
	
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		bulletAnimator.hidden = true;
		gun.enabled = false;
		count = 0;
		done = false;
	}
}
