using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class prisionController : MonoBehaviour
{
	public Transform soldierPrefab;
	
	public RescueZone pController;
	public RescuedSoldiers bController;

	public Helicopter_Controller hController;

	void Update ()
	{
		if (hController.colision == Helicopter_Controller.Colision.Prision && 
            pController.current_soldiers > 0 && 
            !hController.poolSoldiers.activeSelf && hController.thisPrision == pController.gameObject) {

			//position
			hController.poolSoldiers.transform.position = new Vector3(transform.position.x+8.0f, transform.position.y+0.5f, transform.position.z+0.5f);

			//active
			hController.poolSoldiers.SetActive(true);

		}
	}
}

