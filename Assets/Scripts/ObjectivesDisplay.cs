﻿using UnityEngine;
using System.Collections;

public class ObjectivesDisplay : MonoBehaviour {

    public GUIText text;
    public string objective = "Rescue 30 soldiers";
    public string win = "Well done, reporting to headquarters";
    public string loose = "Too bad...";
    public float characters_per_second = 10;
    public float blink_frequency = 0.5f;

    bool writting = false; 
    float time = 0.0f;
    string formed_string = "";

    void Start() {
        startMessage();
    }

    void startMessage() {
        text = GetComponent<GUIText>();
        text.text = "";
        formed_string = "";
        StartCoroutine(writeText(objective));
    }

    void winMessage() {
        text = GetComponent<GUIText>();
        text.text = "";
        formed_string = "";
        StartCoroutine(writeText(win));
    }

    void gameOverMessage() {
        text = GetComponent<GUIText>();
        text.text = "";
        formed_string = "";
        StartCoroutine(writeText(loose));
    }

    void Update() {
        
    }

    IEnumerator writeText(string s) {
        writting = true;
        for (int i = 0; i < s.Length; ++i) {
            formed_string += s[i];
            text.text = formed_string + "_";
            yield return new WaitForSeconds(1.0f / characters_per_second);
        }
        for (int i = 0; i < 10; ++i) {
            yield return new WaitForSeconds(blink_frequency);
            text.text = formed_string + (i % 2 != 0 ? "_" : "");
        }
        text.text = "";
        writting = false;
        yield return null;
    }

}
