﻿using UnityEngine;
using System.Collections;

public class collider_Rescued : MonoBehaviour {

	public GameObject me;

	public BaseController b;
	public RescuedSoldiers RS;
	public RescueZone RZ;

	void OnTriggerEnter(Collider other) {
		if (other.tag == "house") {
			b.liberate();
			RS.freeSoldier();
			me.SendMessage("beSave");
		}
		else if(other.tag == "prision") {
			me.SendMessage("beSave");
		}
	}
}
