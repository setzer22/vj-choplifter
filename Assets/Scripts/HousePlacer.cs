﻿using UnityEngine;
using System.Collections;

public class HousePlacer : MonoBehaviour {

	public Terrain terrain;
	public int target_depth;

	// Use this for initialization
	void Start () {
		Debug.Log("A");
		allAboveGround(gameObject, 0);
	}

	void allAboveGround (GameObject g, int depth) {
		Debug.Log(g.name);
		if (depth == target_depth) {
			g.transform.position = new Vector3(g.transform.position.x, 
			    terrain.SampleHeight(g.transform.position) + 2.60f,
			    g.transform.position.z);
		}
		else {	
			foreach(Transform child in g.transform) {
				GameObject g2 = child.gameObject;
				allAboveGround(g2, depth+1);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
