﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    void OnGUI () {
        GUI.Box(new Rect(10,10,100,90), "Loader Menu");
    
        if(GUI.Button(new Rect(20,40,80,20), "Level 1")) {
			Application.LoadLevel("jungle");
        }
    
        if(GUI.Button(new Rect(20,70,80,20), "Level 2")) {
			Application.LoadLevel("desert");
        }
    }

	void Update() {
		if(Input.GetKey(KeyCode.Space)) Application.LoadLevel ("jungle");
	}
}
