﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {

    public float damage_per_second = 1.0f;
    public float hit_radius = 1.0f;
    public float hit_height = 0.5f;

	public BulletAnimator bAnimator;
	public ParticleSystem hitparticles;

    private LayerMask mask;
    private LayerMask enemy_mask;
    private float max_length;

	void Start () {
        //Masks
        mask = 0x1 << LayerMask.NameToLayer("Terrain");	
        enemy_mask = 0x1 << LayerMask.NameToLayer("Enemy");	
	}
	
	// Update is called once per frame
	void Update () {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo; 
        bool hit = Physics.Raycast(ray, out hitInfo, 100, mask);
        if(hit) {
            Collider[] hitColliders = Physics.OverlapSphere(hitInfo.point, hit_radius, enemy_mask);

            Vector3 lookTo;
            GameObject lock_on_enemy = null;

			Collider min_collider = null;

            //If the laser is near an enemy, we lock onto that enemy
            if (hitColliders.Length > 0) {
                float min = (hitInfo.point - hitColliders[0].transform.position).sqrMagnitude;
                min_collider = hitColliders[0];
                for(int i = 1; i < hitColliders.Length; ++i) {
                    float sq_dist = (hitInfo.point - hitColliders[i].transform.position).sqrMagnitude;
                    if (sq_dist < min) {
                        min = sq_dist;
                        min_collider = hitColliders[i];
                    }
                }
                lookTo = min_collider.transform.position + new Vector3(0,hit_height,0); 
                lock_on_enemy = min_collider.transform.gameObject;
            }
            //Otherwise we just hit the terrain
            else {
                lookTo = hitInfo.point;
            }

            transform.rotation = Quaternion.LookRotation(lookTo - transform.position);
            //TODO: fix the scale here

			//Bullet animation:
			if(Input.GetMouseButton(0)) {
				if(bAnimator.hidden == true) bAnimator.hidden = false; //Shoot!

                //@Note (JSF): Particles should always be shown when shooting the floor
				if(lock_on_enemy != null) {
                    //Do damage
					lock_on_enemy.SendMessage("damage", damage_per_second * Time.deltaTime);

                    hitparticles.transform.position = lock_on_enemy.transform.position;
                    hitparticles.enableEmission = true;
				}
                else {
                    //hitparticles.transform.position = hitInfo.point;
                    hitparticles.enableEmission = false;
                }
			}
			else {
				hitparticles.enableEmission = false;
				bAnimator.hidden = true;
			}
            
        }
	    
	}
}
