﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI_Soldier : MonoBehaviour {

    //The main target for the AI. If it's close enough it will try to aim for it and shoot it.
    public Transform target;

    //The distance from target that triggers the AI behaviour.
    public float trigger_dist;

    //The soldier controller component
    public SoldierController controller;

    //This entity's surrounding space, to check for nearby entities
    public GameObject surrounding_space;

    //The angle in the Z-Y plane between the controlled entity and the target
    private float angle;

    private List<GameObject> nearby_objects;

    private enum State {
        IDLE, AIM, SHOOT, EMPATHIC_SHOOT 
    }
    private State state;

    private const float shoot_angle_L = 35;
    private const float shoot_angle_R = 145;
    private const float shoot_epsilon = 3;

    //Returns true if x is in the range cmp +/- epsilon 
    bool near (float x, float cmp, float epsilon) {
        float top = cmp + epsilon; 
        float bot = cmp - epsilon; 
        return bot <= x && x <= top;
    }

    // Use this for initialization
    void Start () {
        nearby_objects = new List<GameObject>();
    }
    
    // Update is called once per frame
    void Update () {

        Vector3 me = transform.position;
        Vector3 him = target.position;

        //Am I close enough?
        float sq_dist = (him - me).sqrMagnitude;        
        if(sq_dist <= trigger_dist*trigger_dist) {
            state = State.AIM; 
        }
        else {
            state = State.IDLE;
        }

        //Am I able to shoot?
        angle = Vector3.Angle(him - me, Vector3.forward);
        if(near(angle, shoot_angle_L, shoot_epsilon) || near(angle, shoot_angle_R, shoot_epsilon)) {
            state = State.SHOOT;
        }

        //Is someone near me shooting? Then I shoot
        if(state != State.SHOOT) {
            nearby_objects.RemoveAll(obj => obj == null);

            foreach (GameObject o in nearby_objects) { 
                AI_Soldier other = o.GetComponent<AI_Soldier>();
                /*@Debug*/ if (other == null) {print("Nearby object is not a soldier, this should never happen");}
                if(other.state == State.SHOOT) {
                    state = State.EMPATHIC_SHOOT;
                }
            }
        }

        
        //invariant: At this point, angle has the right angle for this update step.
        switch (state) {
            case State.IDLE:
                updateIdle();
                break;
            case State.AIM:
                updateAim();
                break;
            //We need to differentiate between "shoot because I see the helicopter"
            //or "shoot because someone near me sees the helicopter". Otherwise we can get 
            //into a shoot-deadlock
            case State.EMPATHIC_SHOOT:
            case State.SHOOT:
                updateShoot();
                break;
        }
    
    }

    void updateIdle() {
        controller.stop();
    } 

    void updateAim() {
        if(angle < 90) {
            if(angle < shoot_angle_L) controller.walkRight();
            if(angle > shoot_angle_L) controller.walkLeft();
        }
        else {
            if(angle < shoot_angle_R) controller.walkRight();
            if(angle > shoot_angle_R) controller.walkLeft();
        }
    }

    void updateShoot() {
        controller.lookAt(target.position);
        controller.shoot();
    }

    public void add_nearby(GameObject soldier) {
        nearby_objects.Add(soldier);
    }

    public void remove_nearby(GameObject soldier) {
        nearby_objects.Remove(soldier);
        
    }

}
