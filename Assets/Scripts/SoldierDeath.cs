﻿using UnityEngine;
using System.Collections;

public class SoldierDeath : MonoBehaviour {

    public GameObject ragdollPrefab;
    public Transform helicopter;
    public bool dead = false;

    void Start() {
        if(GetComponent<AI_Soldier>()) {
            helicopter = GetComponent<AI_Soldier>().target;
        }
        else if(GetComponent<FlyingSoldierController>()) {
            helicopter = GetComponent<FlyingSoldierController>().target;
        }
    }


    void die() {
        if(dead) return;
        dead = true;

        GameObject ragdoll_instance = (GameObject) Instantiate(ragdollPrefab, transform.position, transform.rotation);
        Rigidbody chest = ragdoll_instance.GetComponentInChildren<Rigidbody>();
        chest.AddForce(100*(transform.position - helicopter.position).normalized, ForceMode.Impulse);
        GameObject.Destroy(gameObject);
    }
}
