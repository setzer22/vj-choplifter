﻿using UnityEngine;
using System.Collections;

public class MissileTest : MonoBehaviour {

    public float speed;
    public float damage;

    public float life_time = 100; 
    float time = 0; 

    public GameObject explosion_prefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if((time += Time.deltaTime) >= life_time) explode();
        transform.position += transform.forward * speed * Time.deltaTime;
	}

    void OnCollisionEnter(Collision c) {
        if (c.collider.tag == "Enemy" || c.collider.tag == "Player") {
            c.collider.transform.gameObject.SendMessage("damage", damage);
            explode();
        }
    }

    void explode() {
        Instantiate(explosion_prefab, transform.position, Quaternion.identity);
        GameObject.Destroy(gameObject);
    }
}
